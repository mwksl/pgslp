import React from 'react';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import App from './containers/App';
import Dashboard from './containers/Dashboard';
import History from './containers/History';
import Profile from './containers/Profile';


const routes = (
  <Router history={createBrowserHistory()}>
      <Route path="/" component={App} />
      <Route path="/user/:userID" component={Profile} />
      <Route path="/history" component={History} />
      <Route path="/dashboard" component={Dashboard} />
      <Route path="*" component={NotFound} />
  </Router>
);

module.exports = routes;
